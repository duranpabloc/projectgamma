import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { NavbarComponent } from './navbar/navbar.component';
import { ContentComponent } from './content/content.component';
import { MainComponent } from './main/main.component';
import { SearchComponent } from './home/search/search.component';
import { ShowcaseComponent } from './shared/components/showcase/showcase.component';
import { InformativeComponent } from './shared/components/informative/informative.component';
import { NewslistComponent } from './shared/components/newslist/newslist.component';
import { SidebarComponent } from './shared/components/sidebar/sidebar.component';
import { RrssComponent } from './shared/components/rrss/rrss.component';
import { ArticuloslistComponent } from './shared/components/articuloslist/articuloslist.component';
import { TwitchlistComponent } from './shared/components/twitchlist/twitchlist.component';
import { ArticuloItemComponent } from './shared/components/articuloslist/articulo-item/articulo-item.component';
import { TwitchItemComponent } from './shared/components/twitchlist/twitch-item/twitch-item.component';
import { NewsItemComponent } from './shared/components/newslist/news-item/news-item.component';
import { TwitterListComponent } from './shared/components/twitter-list/twitter-list.component';
import { NgxTwitterTimelineModule } from 'ngx-twitter-timeline';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NavbarComponent,
    ContentComponent,
    MainComponent,
    SearchComponent,
    ShowcaseComponent,
    InformativeComponent,
    NewslistComponent,
    SidebarComponent,
    RrssComponent,
    ArticuloslistComponent,
    TwitchlistComponent,
    ArticuloItemComponent,
    TwitchItemComponent,
    NewsItemComponent,
    TwitterListComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgxTwitterTimelineModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
