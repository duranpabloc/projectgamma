import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArticuloItemComponent } from './articulo-item.component';

describe('ArticuloItemComponent', () => {
  let component: ArticuloItemComponent;
  let fixture: ComponentFixture<ArticuloItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArticuloItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArticuloItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
