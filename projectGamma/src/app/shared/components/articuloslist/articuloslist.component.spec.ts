import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArticuloslistComponent } from './articuloslist.component';

describe('ArticuloslistComponent', () => {
  let component: ArticuloslistComponent;
  let fixture: ComponentFixture<ArticuloslistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArticuloslistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArticuloslistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
