import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TwitchlistComponent } from './twitchlist.component';

describe('TwitchlistComponent', () => {
  let component: TwitchlistComponent;
  let fixture: ComponentFixture<TwitchlistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TwitchlistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TwitchlistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
