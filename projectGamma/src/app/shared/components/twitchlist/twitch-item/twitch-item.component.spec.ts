import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TwitchItemComponent } from './twitch-item.component';

describe('TwitchItemComponent', () => {
  let component: TwitchItemComponent;
  let fixture: ComponentFixture<TwitchItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TwitchItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TwitchItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
